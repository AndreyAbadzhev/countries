//
//  ServerRequesterCore.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import Alamofire
class ServerRequesterCore {
    
    static func execute<Response: Codable>(request: URLRequest, responseType: Response.Type,
                                           success: @escaping(Response) -> (),
                                           failure: @escaping(CountriesError) -> ()) {
        Alamofire.request(request).responseData(completionHandler: { response in
            let statusCode = response.response?.statusCode ?? 0
            
            switch true {
            case response.result.isFailure: failure(CountriesError(.noInternet))
            case statusCode >= 300 && statusCode <= 500: failure(CountriesError(.unidentified))
            case statusCode >= 500: failure(CountriesError(.serverUnreachable))
            default:
                do {
                    let data = response.result.value ?? Data()
                    let response = try (JSONDecoder().decode(responseType, from: data))
                    success(response)
                } catch {
                    failure(CountriesError(.incorrectIncomingData))
                }
            }
        })
    }
}

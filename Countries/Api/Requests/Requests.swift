//
//  Requests.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import Alamofire
class Requests: CountriesApi {
    
    private func constructRequest(urlString: String,
                                 parameters: [String: String],
                                 method: HTTPMethod,
                                 body: [String: Any]) -> URLRequest? {
        
        var _urlString = urlString
        
        for (key, value) in parameters {
            _urlString += key + "=" + value + "&"
        }
        
        guard let encodedUrlString = (_urlString).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let encodedUrl = URL(string: encodedUrlString) else { return nil }
        
        
        var request: URLRequest = URLRequest(url: encodedUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = method.rawValue
        
        if !body.isEmpty {
            request.httpBody = body.convertToData()
        }
        
        return request
    }
    
}

extension Requests {
    func getCountries(parameters: [String : String],
                      success: @escaping ([Country]) -> (),
                      failure: @escaping (CountriesError) -> ()) {
        
        let urlString = Constants.baseUrlString + "all?"
        guard let request = constructRequest(urlString: urlString,
                                                  parameters: parameters,
                                                  method: .get,
                                                  body: [:]) else { failure(CountriesError(.unidentified)); return }
        
        ServerRequesterCore.execute(request: request, responseType: [Country].self, success: success, failure: failure)
    }
}

extension Requests {
    func getDetailed(countryName: String, parameters: [String : String],
                     success: @escaping ([Country]) -> (),
                     failure: @escaping (CountriesError) -> ()) {
        
        let urlString = Constants.baseUrlString + "name/\(countryName)"
        guard let request = constructRequest(urlString: urlString,
                                                                 parameters: parameters,
                                                                 method: .get,
                                                                 body: [:]) else { failure(CountriesError(.unidentified)); return }
        
        ServerRequesterCore.execute(request: request, responseType: [Country].self, success: success, failure: failure)
    }
}

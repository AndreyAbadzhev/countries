//
//  CountriesApi.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
protocol CountriesApi {
    func getCountries(parameters: [String: String],
                      success: @escaping([Country]) -> (),
                      failure: @escaping(CountriesError) -> ())
    
    func getDetailed(countryName: String,
                     parameters: [String: String],
                     success: @escaping([Country]) -> (),
                     failure: @escaping(CountriesError) -> ())
}

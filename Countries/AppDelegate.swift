//
//  AppDelegate.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var rootCoordinator: RootCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        rootCoordinator = RootCoordinator(window: window)
        rootCoordinator?.start()
        return true
    }
}


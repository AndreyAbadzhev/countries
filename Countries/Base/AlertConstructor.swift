//
//  AlertConstructor.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import UIKit
class AlertConstructor {
    func errorAlert(error: CountriesError) -> UIAlertController {
        let alert = UIAlertController(title: "Oops", message: error.text, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAlertAction)
        return alert
    }
}

//
//  Coordinator.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import UIKit
protocol Coordinator: class {
    var id: CoordinatorId { get }
    var parent: Coordinator? { get }
    var children: [Coordinator] { get set }
    var navigationController: UINavigationController? { get }
}

extension Coordinator {
    func getCoordinatorWithId(id: CoordinatorId) -> Coordinator? {
        for coordinator in children {
            if coordinator.id == id { return coordinator }
        }
        return nil
    }
    
    func addCoordinator(coordinator: Coordinator) {
        children.append(coordinator)
    }
    
    func removeCoordinatorWithId(id: CoordinatorId) {
        children = children.filter { $0.id != id }
    }
    
    func removeAllCoordinators() {
        children.removeAll()
    }
}

enum CoordinatorId {
    case root
    case countryList
    case countryDetailed
}

//
//  CoreDataBase.swift
//  Countries
//
//  Created by iOS on 19/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import CoreData
class CoreDataManager {
    
    
    private init () {
        configure()
    }
    
    static let shared = CoreDataManager()
    
    private var modelName: String = "Countries"
    private var managedObjectModel: NSManagedObjectModel?
    private var persistanceStoreUrl: NSURL?
    private var persistentStoreCoordinator: NSPersistentStoreCoordinator?
    var managedObjectContext: NSManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    
    
    private func configure() {
        guard let modelUrl = Bundle.main.url(forResource: modelName, withExtension: "momd") else { return }
        managedObjectModel = NSManagedObjectModel(contentsOf: modelUrl)
        
        persistanceStoreUrl = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]).appendingPathComponent("\(modelName).sqlite") as NSURL
        
        guard let managedObjectModel = managedObjectModel else { return }
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        do {
            let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at:
                persistanceStoreUrl as URL?, options: options)
        } catch {
            let error = error as NSError
            print(error.localizedDescription)
        }
        self.persistentStoreCoordinator = persistentStoreCoordinator
        
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
    }
    
    private func saveContext(completion: @escaping() -> ()) {
        let privateObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateObjectContext.parent = managedObjectContext
        
        privateObjectContext.perform {
            do {
                try privateObjectContext.save()
                self.managedObjectContext.performAndWait {
                    do {
                        try self.managedObjectContext.save()
                        completion()
                    } catch {
                        fatalError("Failure to save context: \(error)")
                    }
                }
            } catch {
                fatalError("Failure to save context: \(error)")
            }
        }
    }
    
    private func fetchContext<T: BaseCoreDataModel>(responseType: T.Type, result: @escaping([T]?) -> ()) {
        let privateObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateObjectContext.parent = managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: responseType))
        privateObjectContext.perform {
            do {
                result(try privateObjectContext.fetch(fetchRequest) as? [T])
            } catch {
                print(error)
            }
            
        }
    }
    
    func deleteEntity(entityName: String, completion: @escaping() -> ()) {
        let privateObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateObjectContext.parent = managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName )
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try self.persistentStoreCoordinator?.execute(deleteRequest, with: privateObjectContext)
            completion()
        } catch {
            print("Не удалось очистить Core Data от \(entityName)")
        }
    }
    
    func putData<T: BaseModel, D: BaseCoreDataModel>(toSave: [T], coreDataModelType: D.Type, completion: @escaping() -> ()) {
        self.fetchContext(responseType: coreDataModelType, result: { objects in
            guard let objects = objects else { let _ = toSave.map { $0.convertToCoreDataModel() }; self.saveContext(completion: completion); return }
            
            for model in toSave {
                for object in objects {
                    if model.name == object.name { (object as? NSManagedObject)?.setValue(model.name, forKey: "name") }
                }
            }
            completion()
        })
    }
    
    func updateObject<T: BaseModel, D: BaseCoreDataModel>(model: T, coreDataModelType: D.Type) {
        self.fetchContext(responseType: coreDataModelType, result: { objects in
            guard let objects = objects else { return }
            
            for object in objects {
                if model.name == object.name {
                    let _ = model.convertToCoreDataModel()
                    self.saveContext(completion: {})
                    break
                }
            }
        })
    }
    
    
    func getData<T: Codable, D: BaseCoreDataModel>(responseType: T.Type, coreDataModelType: D.Type, completion: @escaping([T]) -> ()) {
        self.fetchContext(responseType: coreDataModelType, result: { objects in
            let response = (objects ?? []).map { $0.convertToModel() } as? [T] ?? []
            completion(response)
        })
    }
}

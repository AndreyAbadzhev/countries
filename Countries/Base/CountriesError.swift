//
//  CountriesError.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
class CountriesError: Error {
    let text: String
    
    init(_ text: String) {
        self.text = text
    }
    
    init(_ error: Error) {
        self.text = error.localizedDescription
    }
    
    init(_ wErrorCode: CountriesErrorCode) {
        self.text = wErrorCode.rawValue
    }
}

enum CountriesErrorCode: String {
    case noInternet = "Check your internet connection"
    case serverUnreachable = "Server is unreachable"
    case incorrectIncomingData = "Incorrect data"
    case unidentified = "Something went wrong"
}

//
//  RealmDataBase.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import RealmSwift
/*class RealmDataBase: RealmManager {
    
    func writeCountries(object: [Country]) {
        DispatchQueue.global(qos: .utility).async {
            let realm: Realm = try! Realm()
            for object in object {
                try! realm.write {
                    realm.create(RealmCountry.self, value: ["name": object.name], update: .modified)
                }
            }
        }
    }
    
    func obtain<T: Object>(objectType: T.Type, success: @escaping(Results<T>?) -> ()) {
        let realm: Realm = try! Realm()
        success(realm.objects(objectType))
    }
    
    func obtainSingleCountry<T: Object>(objectType: T.Type, key: String, success: @escaping(T?) -> ()) {
        let realm: Realm = try! Realm()
        success(realm.object(ofType: objectType, forPrimaryKey: key))
    }
    
    func updateCountry(object: Country) {
        DispatchQueue.global(qos: .utility).async {
            let realm: Realm = try! Realm()
            try! realm.write {
                realm.add(RealmCountry(name: object.name, currencyName: object.currencyName ?? "", countryCode: object.countryCode ?? "", countryFlag: object.countryFlag ?? ""), update: .modified)
            }
        }
    }
}
*/

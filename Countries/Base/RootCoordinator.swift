//
//  RootCoordinator.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import UIKit
class RootCoordinator: Coordinator {
    
    let id: CoordinatorId
    weak var parent: Coordinator? = nil
    var children: [Coordinator] = []
    var navigationController: UINavigationController?
    
    let window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
        self.id = .root
    }
    
    func start() {
        showCountryList()
    }
    
    func showCountryList() {
        let api: CountriesApi = Requests()
        let countryListCoordinator = CountryListCoordinator(parent: self, id: .countryList, api: api)
        addCoordinator(coordinator: countryListCoordinator)
        window?.rootViewController = countryListCoordinator.navigationController
    }
}

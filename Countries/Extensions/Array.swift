//
//  Array.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
extension Array {
    func getElement(index: Int) -> Element? {
        if index >= 0 && index <= self.count - 1 {
            return self[index]
        } else {
            return nil
        }
    }
}

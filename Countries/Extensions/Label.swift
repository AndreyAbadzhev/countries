//
//  Label.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import UIKit
extension UILabel {
    func animatedUpdate(with text: String) {
        UIView.transition(with: self, duration: 0.5, options: .transitionCrossDissolve, animations: { self.text = text; self.sizeToFit() })
    }
}

//
//  VIew.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    func animatedShow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1
        })
    }
    
    func animatedHide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        })
    }
}

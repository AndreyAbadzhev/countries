//
//  CountryModel.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import CoreData
import ObjectMapper
import RealmSwift

/*class RealmCountry: Object {
    
    @objc dynamic var name: String = ""
    @objc dynamic var currencyName: String = ""
    @objc dynamic var countryCode: String = ""
    @objc dynamic var countryFlag: String = ""
    
    lazy var downloadNeeded: Bool = {
        return currencyName.isEmpty ? true : false
    }()
    
    convenience init(name: String, currencyName: String, countryCode: String, countryFlag: String) {
        self.init()
        self.name = name
        self.currencyName = currencyName
        self.countryCode = countryCode
        self.countryFlag = countryFlag
    }
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["downloadNeeded"]
    }
}*/

protocol BaseModel {
    var name: String { get set }
    
    func convertToCoreDataModel() -> NSManagedObject?
}

class Country: Codable, BaseModel {
    
    var name: String
    let countryCode: String?
    let currencies: [Currency]?
    
    lazy var currencyName: String? = { return currencies?.getElement(index: 0)?.name ?? nil }()
    
    lazy var countryFlag: String? = {
        guard let countryCode = countryCode else { return nil }
        var flag = ""
        let base: UInt32 = 127397
        for scalar in countryCode.unicodeScalars {
            if let unicodeScalar = UnicodeScalar(base + scalar.value) { flag.unicodeScalars.append(unicodeScalar) }
        }
        return String(flag)
    }()
    
    var isDownloadNeeded: Bool {
        return currencyName?.isEmpty == true || currencyName == nil ? true : false
    }
    
    init(from: CoreDataCountry) {
        name = from.name
        currencies = nil

        countryCode = from.countryCode
        countryFlag = from.countryFlag
        currencyName = from.currencyName
    }
    
    func convertToCoreDataModel() -> NSManagedObject? {
        guard let entity = NSEntityDescription.entity(forEntityName: "CoreDataCountry", in: CoreDataManager.shared.managedObjectContext) else { return nil }
        let object = CoreDataCountry(name: name, currencyName: currencyName, countryCode: countryCode, countryFlag: countryFlag, entity: entity, insertIntoManagedObjectContext: CoreDataManager.shared.managedObjectContext)
        return object
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case countryCode = "alpha2Code"
        case currencies
    }
}

struct Currency: Codable {
    let name: String?
}

protocol BaseCoreDataModel {
    var name: String { get set }
    
    func convertToModel() -> Codable
}

@objc(CoreDataCountry)
class CoreDataCountry: NSManagedObject, BaseCoreDataModel {
    
    @NSManaged var name: String
    @NSManaged var currencyName: String?
    @NSManaged var countryCode: String?
    @NSManaged var countryFlag: String?
    
    convenience init(name: String, currencyName: String?, countryCode: String?, countryFlag: String?, entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext) {
        self.init(entity: entity, insertInto: context)
        self.name = name
        self.currencyName = currencyName
        self.countryCode = countryCode
        self.countryFlag = countryFlag
    }
    
    func convertToModel() -> Codable {
        return Country.init(from: self)
    }

}

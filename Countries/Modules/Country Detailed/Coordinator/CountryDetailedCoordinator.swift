//
//  CountryDetailedCoordinator.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import UIKit
class CountryDetailedCoordinator: Coordinator {
    
    var id: CoordinatorId
    weak var parent: Coordinator?
    var children: [Coordinator] = []
    var navigationController: UINavigationController?
    let controller = CountryDetailedViewController()
    let country: Country
    let api: CountriesApi
    
    init(parent: Coordinator?,
         id: CoordinatorId,
         navigationController: UINavigationController?,
         country: Country,
         api: CountriesApi)
    {
        self.id = id
        self.parent = parent
        self.navigationController = navigationController
        self.country = country
        self.api = api
        
        configueCountryDetailedModule()
    }
    
    func configueCountryDetailedModule() {
        let presenter = CountryDetailedPresenter()
        let interactor = CountryDetailedInteractor(api: api, country: country)
        
        controller.output = presenter
        
        presenter.view = controller
        presenter.interactor = interactor
        presenter.closeModule = { [weak self] in self?.dissmiss() }
        presenter.showErrorAlert = { [weak self] error in self?.showErrorAlert(error: error) }
    }
    
    func dissmiss() {
        parent?.removeCoordinatorWithId(id: id)
        navigationController?.popViewController(animated: true)
    }
    
    func showErrorAlert(error: CountriesError) {
        controller.present(AlertConstructor().errorAlert(error: error), animated: true, completion: nil)
    }
}

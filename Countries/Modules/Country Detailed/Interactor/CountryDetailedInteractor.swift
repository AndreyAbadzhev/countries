//
//  CountryDetailedInteractor.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
class CountryDetailedInteractor: CountryDetailedInteractorInput {
    
    let api: CountriesApi
    var country: Country
    
    var countryName: String {
        return country.name
    }
    
    init(api: CountriesApi, country: Country) {
        self.api = api
        self.country = country
    }
    
    func getCountry(success: @escaping(Country?) -> (), failure: @escaping(CountriesError) -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            self.getCountryFromCoreData(success: { country in DispatchQueue.main.async { success(country) } },
                                        failure: { [weak self] in self?.getCountryFromServer(success: success, failure: failure)
            })
        }
    }
    
    func getCountryFromCoreData(success: @escaping(Country) -> (), failure: @escaping() -> ()) {
        CoreDataManager.shared.getData(responseType: Country.self, coreDataModelType: CoreDataCountry.self, completion: { countries in
            for country in countries {
                if country.name == self.country.name && !country.isDownloadNeeded { success(country); return }
            }
            failure()
        })
    }
    
    func getCountryFromServer(success: @escaping(Country?) -> (), failure: @escaping(CountriesError) -> ()) {
        api.getDetailed(countryName: country.name, parameters: [:], success: { countries in
            guard let country = countries.getElement(index: 0) else { return }
            CoreDataManager.shared.updateObject(model: country, coreDataModelType: CoreDataCountry.self)
            success(country)
        }, failure: failure)
    }
    
    
}


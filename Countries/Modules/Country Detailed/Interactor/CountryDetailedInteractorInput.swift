//
//  CountryDetailedInteractorInput.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
protocol CountryDetailedInteractorInput {
    var countryName: String { get }
    
    func getCountry(success: @escaping(Country?) -> (), failure: @escaping(CountriesError) -> ())
}

//
//  CountryDetailedPresenter.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
class CountryDetailedPresenter: CountryDetailedViewOutput {
    
    weak var view: CountryDetailedViewInput?
    var interactor: CountryDetailedInteractorInput?
    
    var closeModule: (() -> ())?
    var showErrorAlert: ((CountriesError) -> ())?
    
    var countryName: String? {
        return interactor?.countryName
    }
    
    var country: Country? {
        didSet { view?.updateState() }
    }
    
    func viewDidLoad() {
        interactor?.getCountry(success: { [weak self] country in
            self?.country = country
        }, failure: { [weak self] error in
            self?.showErrorAlert?(error)
        })
    }
    
    func backButtonDidTap() {
        closeModule?()
    }
}

//
//  CountryDetailedView.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import UIKit

class CountryDetailedViewController: UIViewController, CountryDetailedViewInput {

    @IBOutlet weak var currencyNameLabel: UILabel!
    var titleLabel: UILabel?
    
    var output: CountryDetailedViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        output?.viewDidLoad()
    }
    
    func setupInitialState() {
        prepareNavigationBar()
    }
    
    func prepareNavigationBar() {
        self.titleLabel = self.navigationItem.setSmallLabel()
        titleLabel?.text = output?.countryName ?? "No Data"

        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backButtonDidTap))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    func updateState() {
        if let countryName = output?.country?.name, let countryFlag = output?.country?.countryFlag {
            titleLabel?.animatedUpdate(with: "\(countryName) \(countryFlag)")
        }
        
        currencyNameLabel.animatedUpdate(with: "Country corrency: \(output?.country?.currencyName ?? "No data")")
    }
    
    @objc func backButtonDidTap() {
        output?.backButtonDidTap()
    }
}

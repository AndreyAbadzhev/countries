//
//  CountryDetailedViewOutput.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
protocol CountryDetailedViewOutput {
    var countryName: String? { get }
    var country: Country? { get }
    
    func viewDidLoad()
    
    func backButtonDidTap()
}

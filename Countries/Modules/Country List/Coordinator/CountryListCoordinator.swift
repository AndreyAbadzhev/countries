//
//  CountryListCoordinator.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
import UIKit
class CountryListCoordinator: Coordinator {
    let id: CoordinatorId
    weak var parent: Coordinator?
    var children: [Coordinator] = []
    var navigationController: UINavigationController?
    let controller = CountryListViewController()
    let api: CountriesApi

    
    init(parent: Coordinator?, id: CoordinatorId, api: CountriesApi) {
        self.id = id
        self.parent = parent
        self.api = api
        
        configueCountryListModule()
    }
    
    func configueCountryListModule() {
        
        let presenter = CountryListPresenter()
        let interactor = CountryListInteractor(api: api)
        
        controller.output = presenter
        
        presenter.view = controller
        presenter.interactor = interactor
        presenter.showCountryDetailed = { [weak self] country in self?.showCountryDetailed(country) }
        presenter.showErrorAlert = { [weak self] error in self?.showErrorAlert(error: error) }
        
        self.navigationController = UINavigationController(rootViewController: controller)
    }
    
    func showCountryDetailed(_ country: Country) {
        let countryDetailedCoordinator = CountryDetailedCoordinator(parent: self, id: .countryList, navigationController: navigationController, country: country, api: api)
        addCoordinator(coordinator: countryDetailedCoordinator)
        navigationController?.pushViewController(countryDetailedCoordinator.controller, animated: true)
    }
    
    func showErrorAlert(error: CountriesError) {
        controller.present(AlertConstructor().errorAlert(error: error), animated: true, completion: nil)
    }
}

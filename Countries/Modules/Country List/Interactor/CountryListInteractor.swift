//
//  CountryListInteractor.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
class CountryListInteractor: CountryListInteractorInput {
    
    let api: CountriesApi
    
    init(api: CountriesApi) {
        self.api = api
    }
    
    func getCountries(success: @escaping ([Country]) -> (), failure: @escaping(CountriesError) -> ()) {
        
        getCountriesFromCoreData(completion: { [weak self] countries in
            DispatchQueue.main.async { success(countries.sorted { $0.name > $1.name }) }
            self?.getCountriesFromServer(success: { [weak self] countries in
                DispatchQueue.main.async { success(countries) }
                self?.putCountriesToCoreData(toSave: countries)
            }, failure: { error in
                DispatchQueue.main.async { failure(error) }
            })
        })
       
    }
    
    
    func getCountriesFromCoreData(completion: @escaping([Country]) -> ()) {
        CoreDataManager.shared.getData(responseType: Country.self, coreDataModelType: CoreDataCountry.self, completion: completion)
    }
    
    func getCountriesFromServer(success: @escaping([Country]) -> (), failure: @escaping(CountriesError) -> ()) {
        let parameters = ["fields": "name"]
        api.getCountries(parameters: parameters, success: success, failure: failure)
    }
    
    func putCountriesToCoreData(toSave: [Country]) {
        CoreDataManager.shared.putData(toSave: toSave, coreDataModelType: CoreDataCountry.self, completion: {})
    }
}

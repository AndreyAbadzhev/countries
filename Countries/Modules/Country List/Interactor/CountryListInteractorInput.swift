//
//  CountryListInteractorInput.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
protocol CountryListInteractorInput {
    func getCountries(success: @escaping ([Country]) -> (), failure: @escaping(CountriesError) -> ())
}

//
//  CountryListPresenter.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
class CountryListPresenter: CountryListViewOutput {
    
    weak var view: CountryListViewInput?
    var interactor: CountryListInteractorInput?
    
    var showCountryDetailed: ((Country) -> ())?
    var showErrorAlert: ((CountriesError) -> ())?
    
    var countries: [Country] = [] {
        didSet { view?.updateState() }
    }
    
    func viewIsReady() {
        interactor?.getCountries(success: { [weak self] countries in
            self?.countries = countries
        }, failure: { [weak self] error in
            self?.view?.updateState()
            self?.showErrorAlert?(error)
        })
    }
    
    func cellDidTap(index: Int) {
        guard let country = countries.getElement(index: index) else { return }
        showCountryDetailed?(country)
    }
    
}

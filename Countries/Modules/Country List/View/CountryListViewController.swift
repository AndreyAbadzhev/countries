//
//  CountryListView.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import UIKit

class CountryListViewController: UIViewController, CountryListViewInput {

    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    var output: CountryListViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        activityView.animatedShow()
        output?.viewIsReady()
    }
    
    func setupInitialState() {
        activityView.startAnimating()
        tableView.refreshControl = UIRefreshControl()
        tableView.register(UINib(nibName: String(describing: CountryCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CountryCell.self))
        tableView.register(UINib(nibName: "ErrorCell", bundle: nil), forCellReuseIdentifier: "ErrorCell")
    }
    
    func updateState() {
        activityView.animatedHide()
        tableView.reloadData()
    }
}

extension CountryListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output?.countries.count == 0 ? 1 : output?.countries.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let country = output?.countries.getElement(index: indexPath.row) else {
            return tableView.dequeueReusableCell(withIdentifier: "ErrorCell") ?? UITableViewCell()
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CountryCell.self)) as? CountryCell else { return UITableViewCell() }
        cell.fillCell(with: country)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return output?.countries.count == 0 ? tableView.bounds.height : 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output?.cellDidTap(index: indexPath.row)
    }
}

extension CountryListViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if tableView.refreshControl?.isRefreshing == true {
            tableView.refreshControl?.endRefreshing()
            output?.viewIsReady()
        }
    }
}

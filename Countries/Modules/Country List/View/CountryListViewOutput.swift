//
//  CountryListViewOutput.swift
//  Countries
//
//  Created by Андрей Абаджев on 10/09/2019.
//  Copyright © 2019 Андрей Абаджев. All rights reserved.
//

import Foundation
protocol CountryListViewOutput {
    var countries: [Country] { get }

    func viewIsReady()
    
    func cellDidTap(index: Int)
}
